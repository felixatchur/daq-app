import React, {Component} from 'react';
import './App.css';
import Map from './commonpath/components/Map';
import CommonPathTool from './commonpath/containers/CommonPathTool/CommonPathTool';

export default class App extends Component {
  render(){
    return (
      <div>
        <CommonPathTool />
      </div>
    )
  }
}