import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
// import * as actionTypes from '../../redux/actionTypes';
import ReactDOM from 'react-dom';

class PointsCollectionTool extends Component {
    constructor(props){
        super(props);
        this.state = {
            ok: true,
            heatmapLayer: null,
            finishButtonStatus: true,
        }
        this.pointsList = [];
        this.polyline = null;
        this.pointsToSubmit = [];
    }

    getRadiusDistance(x){
        return x * Math.PI / 180;
    };

    getDistanceBetweenPoints(p1, p2){
        const R = 6378137;
        const dLat = this.getRadiusDistance(p2.lat - p1.lat);
        const dLng = this.getRadiusDistance(p2.lng - p1.lng);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.getRadiusDistance(p1.lat)) * Math.cos(this.getRadiusDistance(p2.lat))
        * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c;
        return d;
    }

    generatePoints(point1, point2){
        const pointsList = [];
        const distanceBetweenPoints = this.getDistanceBetweenPoints(point1, point2)
        const pointsNumber = Math.floor(distanceBetweenPoints/3-1);
        const unitLat = (point2.lat - point1.lat)/pointsNumber;
        const unitLng = (point2.lng - point1.lng)/pointsNumber;
        pointsList.push(point1);
        for(let i=1; i<pointsNumber; i++){
            const point = {lat: point1.lat+unitLat*i, lng: point1.lng+unitLng*i};
            pointsList.push(point);
        }
        pointsList.push(point2);
        pointsList.forEach(point=>{
            const marker = new window.google.maps.Marker({
                position: point,
                icon: {
                    path: window.google.maps.SymbolPath.CIRCLE,
                    scale: 1.5
                }
            })
            marker.setMap(this.props.map)
            this.pointsToSubmit.push(marker);
        })
    }

    onChange(checked){
        console.log(`Switch to ${checked}`);
    }

    onFinishButton(){
        window.google.maps.event.clearListeners(this.props.map, 'click');
        for(let i=0; i<this.pointsList.length-1; i++){
            this.generatePoints(this.pointsList[i], this.pointsList[i+1])
        }
        this.pointsList = [];
        if(this.polyline){
            this.polyline.setMap(null);
        }
    }

    onSubmitButton(){
        if(this.pointsToSubmit.length>0){
            console.log(this.pointsToSubmit);
        }
        this.pointsToSubmit=[];
    }

    onStart(){
        this.props.map.addListener('click', (event) => {
        const clickedPoint = {lat: event.latLng.lat(), lng: event.latLng.lng()};
        this.pointsList.push(clickedPoint)
        const polylinePath = new window.google.maps.Polyline({
            path: this.pointsList,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        })
        if(this.polyline){
            this.polyline.setMap(null);
        }
        this.polyline = polylinePath;
        polylinePath.setMap(this.props.map)
        })
    }

    onClear(){
        if(this.pointsToSubmit){
            this.pointsToSubmit.forEach(point=>{
                point.setMap(null);
            })
        }
        if(this.polyline){
            this.polyline.setMap(null);
            this.polyline = null;
        }
        this.pointsList=[];
        this.pointsToSubmit=[];
    }

    __init() {
        const startButton = (
            <Button type="primary" style={{"position":"absolute", "top": "15PX", "right":"220px"}} onClick={this.onStart.bind(this)}>Start</Button>
        )
        const finishButton = (
            <Button type="primary" style={{"position":"absolute", "top":"15px", "right":"120px"}} onClick={this.onFinishButton.bind(this)}>Finish</Button>
        )
        const clearButton = (
            <Button type="danger" style={{"position":"absolute", "top": "15PX", "right":"20px"}} onClick={this.onClear.bind(this)}>Clear</Button>

        )
        const submitButton = (
            <div style={{"textAlign": "right", "height": 50, "position":"relative", "top":"10px", "right":"30px"}}>
                {this.state.finishButtonStatus?<Button type="primary" onClick={this.onSubmitButton.bind(this)}>Submit</Button> : null}
            </div>
        )
        ReactDOM.render([startButton, finishButton, clearButton, submitButton], document.getElementById('startButton'))
    }

    componentDidUpdate(){
        if(this.props.status && this.props.map){
            this.__init();
        }
    }

    render(){
        return(null)
    }
}

const mapStateToProps = (state) => {
    return {
        map: state.commonpath.map
    }
}

const mapDispatchToProps = dispatch => {
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PointsCollectionTool)
