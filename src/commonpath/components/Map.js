import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../redux/actionTypes';
import style from "./Map.css";
import Aux from '../hocs/Auxiliary/Auxiliary';

class Map extends Component {
    constructor(props){
        super(props);
        this.state = {
            ok: true,
            heatmapLayer: null,
            finishButtonStatus: true,
        }
    }

    __init() {
      const map = new window.google.maps.Map(document.getElementById('map'),{
        center: this.props.center,
        zoom: this.props.zoom,
        zoomControl: true,
        scrollwheel: false,
        streetViewControl: false,
        mapTypeControl: false,
        mapTypeId: 'roadmap',
      });
      
      this.props.onMapInit(map)

      if (this.state.ok !== true) {
        this.setState({
          ...this.state,
          ok: true
        })
      }
    }

    componentDidMount(){
        this.__init();
    }

    render () {
        return (
            <div>                
                <div style={{"textAlign": "center", "height": 60}}>
                    <span style={{"fontWeight": "bold", "fontSize": "26px"}}>Commonpath Planning Tool:  </span>
                </div>
                <Aux>
                    <div className={style.maps} id="map" />
                    {this.props.children}
                </Aux>
                <div id="startButton" className="clicked-coordinate">
                </div>
                <div id="submitButton" />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        map: state.commonpath.map,
        center: state.commonpath.center,
        zoom: state.commonpath.zoom,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onMapInit: (map) => dispatch({type: actionTypes.INIT_MAP, payload:{map: map}}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Map);
