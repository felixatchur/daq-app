import * as actionTypes from './actionTypes';
import { updateObject } from '../shared/utility';

const initialState = {
    map: null,
    networkId: 326046,
    center: {lat: -33.879944, lng: 151.203373},
    zoom: 19
}

const init = (state, action) => {
    return updateObject( state, {
        map: action.payload.map
    })
}

const reducer = ( state = initialState, action ) => {
    switch (action.type){
        case actionTypes.INIT_MAP: return init(state, action);
        default:
            return state;
    }
}

export default reducer;