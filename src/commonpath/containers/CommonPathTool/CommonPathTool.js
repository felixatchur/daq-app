import React, { Component } from 'react';
import Map from '../../components/Map';
import PointsCollectionTool from '../../components/PointsCollectionTool/PointsCollectionTool';

class CommonPathTool extends Component {

    render(){
        return(
            <Map>
                <PointsCollectionTool status={true}/>
            </Map>
        )
    }
}

export default CommonPathTool;