import { combineReducers } from 'redux';
import commonpath from '../commonpath/redux/reducer';

const rootReducer = combineReducers({
    commonpath: commonpath,
})

export default rootReducer;
